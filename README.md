# Binary To Array Of Byte

Generate an array of bytes and stores it in an easy to use header file from a binary file on disk.

![](https://image.prntscr.com/image/mLg9tFAuTKuDdA8GTt8xKA.png)

## Why?

This code was requested.  Maybe you don't want to ship your .exe with additional files or want to load the file from memory for obscurity.

## Usage

Drag and drop your binary onto BinaryToAOB.exe and the generated header file will be placed in the same folder as the binary.

https://guidedhacking.com/threads/binary-to-array-of-bytes-dumper.9340/

## TODO

## Credits

All the good folks at https://guidedhacking.com